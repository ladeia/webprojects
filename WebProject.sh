#!/bin/bash 

echo "Create WebProject $1"

if [ -d $1 ]; then
		echo "Não é possivel criar o projeto $1. Já existe uma pasta com mesmo nome."
		exit 1
	else
		mkdir $1
fi

cd $1
mkdir "images"
mkdir "css"
mkdir "js"

echo "<!DOCTYPE HTML>
<html lang=\"pt-br\">
	<head>
		<meta charset=\"UTF-8\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"css/stylesheet.css\">
		<title></title>
	</head>
	<body>
		<script src=\"js/main.js\" type=\"text/javascript\">
	</body>
</html>" >> index.html
echo "html created"

echo "/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}" >> css/stylesheet.css
echo "css created"

echo "" >> js/main.js
echo "js created"

echo
echo "Project created!" 
exit 0